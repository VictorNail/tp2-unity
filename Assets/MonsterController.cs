using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Unity.VisualScripting;
using UnityEngine;

public class MonsterController : MonoBehaviour
{
    private float Life=1f;
    private float Stat = 10f;
    private float wave = 1;
    public GameObject Player;
    public float SpawnRate = 3F;
    private float NextSpawn;
    private float ClickNumber = 0f;
    private TMPro.TextMeshProUGUI nbClickText;

    private TMPro.TextMeshProUGUI WaveText;


    private SpriteRenderer circleRenderer;
    public float minChangeInterval = 1f;
    public float maxChangeInterval = 5f;
    private float nextChangeTime;


    void Start()    
    {
        Debug.Log("start Monster");
        WaveText = GameObject.Find("Niveau").GetComponent<TMPro.TextMeshProUGUI>();
        nbClickText = GameObject.Find("Nombre de clique").GetComponent<TMPro.TextMeshProUGUI>();
        nbClickText.text = "Nombre de clique : " + ClickNumber;
        reset();
        circleRenderer = GetComponent<SpriteRenderer>();

        nextChangeTime = Time.time + UnityEngine.Random.Range(5, 400); 


    }

    void Update()
    {
        float degat = 0;
        if (Time.time > NextSpawn)
        {
            NextSpawn = Time.time + SpawnRate;

             degat = Player.GetComponent<playerController>().getDegatPassif();
        }

        if(nextChangeTime <= Time.time) {
            ChangeRandomColor();

            // Met � jour le temps pour le prochain changement al�atoire
            nextChangeTime = Time.time + UnityEngine.Random.Range(5, 100);
        }


        if (Input.GetMouseButtonDown(0))
        {
            ClickNumber++;
            nbClickText.text = "Nombre de clique : " + ClickNumber;
            degat += Player.GetComponent<playerController>().getDegat();
            Debug.Log(degat);
        }
        takeDamage(degat);

        if (isDead())
        {
            reset();
        }

        ChangeRandomColor();

    }

    void ChangeRandomColor()
    {
        Color newColor = new Color(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value);

        circleRenderer.color = newColor;
    }

    public Boolean isDead()
    {
        return Life>0f ? false : true;
    }

    public void takeDamage(float degat)
    {
        Life = Life - degat;
    }

    public void reset()
    {
        if(wave %5f == 0f)
        {
            Life = Stat * 10f;

        }
        else
        {
            Life = Stat;
        }
        Stat *= 1.3f ;
        wave++;

        Player.GetComponent<playerController>().addMoney(5f);

        WaveText.text = "Niveaux : " + wave;
    }

    public float getStat()
    {
        return Stat;
    }

    public void setStat(float newValue)
    {
        Stat = newValue;
    }

    public float getWave()
    {
        return wave;
    }

    public void setWave(float newValue)
    {
        wave = newValue;
    }


    public float getClickNumber()
    {
        return ClickNumber;
    }

    public void setClickNumber(float newValue)
    {
        ClickNumber = newValue;
    }

}
