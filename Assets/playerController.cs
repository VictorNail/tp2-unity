using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour
{
    private float Money = 0f;

    private float Degat = 5f;
    private float DegatPassif =1f;

    private TMPro.TextMeshProUGUI DegatText;
    private TMPro.TextMeshProUGUI DegatPassifText;
    private TMPro.TextMeshProUGUI MoneyText;




    // Start is called before the first frame update
    void Start()
    {
        Money = 0f;
        Debug.Log("start Player");
        DegatText = GameObject.Find("Degat").GetComponent<TMPro.TextMeshProUGUI>();
        DegatPassifText = GameObject.Find("Degat Passif").GetComponent<TMPro.TextMeshProUGUI>();
        MoneyText = GameObject.Find("Argent").GetComponent<TMPro.TextMeshProUGUI>();
        DegatText.text = "D�gat : " + Degat;
        DegatPassifText.text = "D�gat passif : " + DegatPassif;
        MoneyText.text = "Argent : " + Money;
    }

    public float getDegat()
    {
        return Degat;
    }

    public float getDegatPassif()
    {
        return DegatPassif;
    }
    
    public void addDegat(float newDegat)
    {
        Degat += newDegat;
        DegatText.text = "D�gat : " + Degat;
    }

    public void addDegatPassif(float newDegat)
    {
        DegatPassif += newDegat;
        DegatPassifText.text = "D�gat passif : " + DegatPassif;
    }

    // Update is called once per frame

    void Update()
    {
        //MoneyText.text = "Argent : " + Money;
    }

    public void addMoney(float amount)
    {
        Debug.Log("first"+Money);
        Money += amount;
        Debug.Log("second" + Money);

        GameObject.Find("Argent").GetComponent<TMPro.TextMeshProUGUI>().text = "Argent : " + Money;
    }


    public void purchaseDegat()
    {
        if (Money > 50f)
        {
            Degat += 5f;
            Money -= 50f;
        }
    }

    public void purchaseDegatPassif()
    {
        if(Money> 50f)
        {
            DegatPassif += 5f;
            Money -= 50f;
        }
    }

    public float getMoney()
    {
        return Money;
    }

    public void setMoney(float money)
    {
        Money = money;
    }

    public void setDegat(float degat)
    {
        Degat = degat;
    }

    public void setDegatPassif(float degatP)
    {
        DegatPassif = degatP;
    }

}
