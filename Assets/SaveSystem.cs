using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SaveSystem : MonoBehaviour
{
    public float Money;
    public float wave;
    public float degat;
    public float degatPassif;
    public float Stat ;
    public float clickNumber;

    public GameObject Player;
    public GameObject Monster;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            Save();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            Load();
        }
    }

    private void Save()
    {
        degat = Player.GetComponent<playerController>().getDegat();
        degatPassif = Player.GetComponent<playerController>().getDegatPassif();
        Money = Player.GetComponent<playerController>().getMoney();
        Stat = Monster.GetComponent<MonsterController>().getStat();
        clickNumber = Monster.GetComponent<MonsterController>().getClickNumber();
        wave = Monster.GetComponent<MonsterController>().getWave();

        PlayerPrefs.SetFloat("Money", Money);
        PlayerPrefs.SetFloat("wave", wave);
        PlayerPrefs.SetFloat("degat", degat);
        PlayerPrefs.SetFloat("degatPassif", degatPassif);
        PlayerPrefs.SetFloat("Stat", Stat);
        PlayerPrefs.SetFloat("clickNumber", clickNumber);

    }

    private void Load()
    {
        Money = PlayerPrefs.GetFloat("Money", 0);
        wave = PlayerPrefs.GetFloat("wave", 0);
        degat = PlayerPrefs.GetFloat("degat", 0);
        degatPassif = PlayerPrefs.GetFloat("degatPassif", 0);
        Stat = PlayerPrefs.GetFloat("Stat", 0);

        Player.GetComponent<playerController>().setDegat(degat);
        Player.GetComponent<playerController>().setDegatPassif(degatPassif);
        Player.GetComponent<playerController>().setMoney(Money);
        Monster.GetComponent<MonsterController>().setStat(Stat);
        Monster.GetComponent<MonsterController>().setClickNumber(clickNumber);
        Monster.GetComponent<MonsterController>().setWave(wave);
    }
}
